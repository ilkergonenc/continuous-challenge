# Iceberg Digital - Frontend Development Challenge 
Real estate appointment management VueJs application, where you can create, list, update appointments with map integration for calculating estimated time schedules. 

## Quick Start
To run everything:
* Install [Node.js](http://nodejs.org)
* Install [npm](http://npmjs.org/)

From the command line:
```
git clone https://gitlab.com/ilkergonenc/iceberg-frontend-challenge.git
cd iceberg-frontend-challenge
npm install
npm run serve
```

In the browser:
```
http://localhost:8000/
```

## Services
- [Airtable](https://airtable.com/) is a spreadsheet-database service.
- [PostcodesIO](https://postcodes.io/) is a open-source postcode & geolocation service.
- [MapQuest](https://developer.mapquest.com/documentation/) is a maps, routes, directions service.

## Dependencies
- [Axios](https://axios-http.com/) is promise based HTTP client for the browser and node.js,
- [Leaflet](https://leafletjs.com/) id an open-source JavaScript library for interactive maps.
- [Moment](https://momentjs.com/) is a JavaScript date library.

### Usage
On create & update pages, you can either click on map or write postcode in the form they are linked to each other and both will draw a route between. When you click on maps you need to be precisely to get a correct postcode. After you select or fill your location and time valid, it calculates the estimated time plan.