import axios from 'axios'

const http = axios.create({
  baseURL: 'https://api.airtable.com/v0/appgykZBGTF92MnHu/',
  headers: {
    'Content-type': 'application/json',
    'Authorization': 'Bearer keyTzG2IiL3H5C4sK'
  }
})

export default {
  select(table, params) {
    return http.get(table, {
      params: params
    })
  },
  find(table, id) {
    return http.get(`${table}/${id}`)
  },
  getFullName(table, id) {
    return http.get(`${table}/${id}`).then(response => {
      if (table=='Agents') return `${response.data.fields.agent_name} ${response.data.fields.agent_surname}`
      if (table=='Contacts') return `${response.data.fields.contact_name} ${response.data.fields.contact_surname}`
    })
  },
  create(table, data) {
    return http.post(table, data)
  },
  update(table, data) {
    return http.patch(table, data)
  }
}