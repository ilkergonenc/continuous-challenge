import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '@/views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/appointments',
    name: 'Appointments',
    component: () => import('@/views/Appointments/Index.vue'),
  },
  {
    path: '/appointments/create',
    name: 'CreateAppointment',
    component: () => import('@/views/Appointments/Create.vue')
  },
  {
    path: '/appointments/view/:id',
    name: 'ViewAppointment',
    component: () => import('@/views/Appointments/View.vue')
  },
  {
    path: '/appointments/update/:id',
    name: 'UpdateAppointment',
    component: () => import('@/views/Appointments/Update.vue')
  },
  {
    path: '/appointments/agent/:id',
    name: 'AgentAppointments',
    component: () => import('@/views/Appointments/Agent.vue')
  }
]

const router = createRouter({
  linkExactActiveClass: "active",
  history: createWebHashHistory(),
  routes
})

export default router
